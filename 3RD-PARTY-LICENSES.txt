The licenses for third party works used by the web UI code are listed separately
under bin/html/3RD-PARTY-LICENSES.txt

Kotlin Standard Library https://kotlinlang.org/ is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

Ktor https://ktor.io/ is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

kotlinx.serialization library https://github.com/Kotlin/kotlinx.serialization is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

EdDSA-Java https://github.com/str4d/ed25519-java is licensed under the
CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/

BLAKE2b https://github.com/rfksystems/blake2b is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

MapDB http://www.mapdb.org/ is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

SLF4J https://www.slf4j.org/ is licensed under the
MIT license: https://www.slf4j.org/license.html

kotlin-logging https://github.com/MicroUtils/kotlin-logging is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

jtorctl https://github.com/akwizgran/jtorctl is licensed under the
3-Clause BSD License: https://github.com/akwizgran/jtorctl/blob/master/LICENSE

Konfig https://github.com/npryce/konfig is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

Guava https://github.com/google/guava is licensed under the
Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0

weupnp https://bitletorg.github.io/weupnp/ is licensed under the
GNU LGPL 2.1 or later: https://www.gnu.org/licenses/lgpl-2.1.html

Bouncy Castle https://bouncycastle.org/ is licensed under the
MIT license: https://bouncycastle.org/licence.html
